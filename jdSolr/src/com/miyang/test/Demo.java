package com.miyang.test;

import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

public class Demo {

	
	@Test
	public void addDocument() throws Exception{
		
		SolrServer solrServer= new HttpSolrServer("http://localhost:8080/solr/");
		
		SolrInputDocument document= new SolrInputDocument();
		document.addField("id", "12560");
		document.addField("title_ik","手表");
		document.addField("content_ik","贼拉风");
		solrServer.add(document);
		solrServer.commit();
	}
	
	@Test
	public void testDelete() throws Exception{
		
		SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/");
		
		solrServer.deleteById("12560");
		solrServer.commit();
		
	}
	
	
	@Test
	public void testUpdate() throws Exception{
		
		SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/");
		
		SolrInputDocument document= new SolrInputDocument();
		/*
		 * 根据id进行修改
		 */
		document.addField("id", "12560");
		document.addField("title_ik","无与伦比的美丽");
		
		solrServer.add(document);
		solrServer.commit();
	}
	
	@Test
	public void testQuery() throws Exception{
		SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/");
		SolrQuery solrQuery= new SolrQuery();
		solrQuery.setQuery("id:12560");
		//solrQuery.set("q","id:12560");
		QueryResponse query = solrServer.query(solrQuery);
		
		SolrDocumentList results = query.getResults();
		
		System.out.println("共查询到:"+results.getNumFound());
		
		for (SolrDocument solrDocument : results) {
			System.out.println(solrDocument.get("title_ik"));
		}
		
		
	}
	
	
	@Test
	public void testQuery1() throws Exception{
		SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/");
		SolrQuery solrQuery= new SolrQuery();
		solrQuery.setQuery("product_price:[180 TO *]");
		QueryResponse query = solrServer.query(solrQuery);
		SolrDocumentList results = query.getResults();
		System.out.println("共查询到:"+results.getNumFound());
		for (SolrDocument solrDocument : results) {
			
			System.out.println(solrDocument.get("id"));
			System.out.println(solrDocument.get("product_name"));
			System.out.println(solrDocument.get("product_price"));
			System.out.println(solrDocument.get("product_catalog_name"));
			
		}
	}
	
	
	@Test
	public void testComplectedQuery() throws Exception{
		SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/");
		SolrQuery solrQuery= new SolrQuery();
		//查询对象
		solrQuery.setQuery("台灯");
		//默认搜索域
		solrQuery.set("df","product_keywords");
		
		//设置过滤条件
		solrQuery.setFilterQueries("product_catalog_name:雅致灯饰");
		
		//设置排序条件
		solrQuery.setSort("product_price",ORDER.asc);
		
		//设置分页
		solrQuery.setStart(0);
		solrQuery.setRows(5);
		
		//设置结果域中的列表
		solrQuery.setFields("id","product_name","product_price","product_catalog_name","product_picture");
		
		//设置高亮
		solrQuery.setHighlight(true); //打开高亮开关
		solrQuery.addHighlightField("product_name"); //设置高亮域
		solrQuery.setHighlightSimplePre("<span style='color:red'>");
		solrQuery.setHighlightSimplePost("</span>");
		
		
		QueryResponse query = solrServer.query(solrQuery);
		SolrDocumentList results = query.getResults();
		
		Map<String, Map<String, List<String>>> highlighting = query.getHighlighting();
		
		System.out.println("共查询到:"+results.getNumFound());
		for (SolrDocument solrDocument : results) {
			
			String product_name="";
			if(solrDocument.get("id")!=null){
				List<String> list = highlighting.get(solrDocument.get("id")).get("product_name");
				
				if(list != null ){
					product_name=list.get(0);
				}else{
					product_name=(String) solrDocument.get("product_name");
				}
			}
			
			System.out.println(solrDocument.get("id"));
			System.out.println(product_name);
			System.out.println(solrDocument.get("product_price"));
			System.out.println(solrDocument.get("product_picture"));
			System.out.println(solrDocument.get("product_catalog_name"));
			System.out.println("---------------------------------------");
		}
	}
	
	
	
}
