package com.miyang.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.miyang.domain.ProductModel;
import com.miyang.service.ProductService;

@Controller
public class ProductAction {

	@Autowired
	private  ProductService productService;
	
	@RequestMapping("list")
	public String queryByCondition(String queryString,String catalog_name,
			String price,String sort,Model model){
		
		List<ProductModel> productList=productService.queryByCondition(queryString,catalog_name,price,sort);
		
		model.addAttribute("productList", productList);
		model.addAttribute("catalog_name",catalog_name);
		model.addAttribute("queryString",queryString);
		model.addAttribute("price",price);
		model.addAttribute("sort",sort);
		return "product_list";
	}
}
