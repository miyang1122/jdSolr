package com.miyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miyang.dao.ProductDao;
import com.miyang.domain.ProductModel;
import com.miyang.service.ProductService;

@Service("productService")
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao productDao;
	
	public List<ProductModel> queryByCondition(String queryString, String catalog_name, String price, String sort) {
		
		return productDao.queryByCondition(queryString,catalog_name,price,sort);
	}

}
