package com.miyang.service;

import java.util.List;

import com.miyang.domain.ProductModel;

public interface ProductService {

	/**
	* 查询方法
	*/
	List<ProductModel> queryByCondition(String queryString, String catalog_name, String price, String sort);

	//测试方法

}
