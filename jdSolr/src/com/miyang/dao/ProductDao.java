package com.miyang.dao;

import java.util.List;

import com.miyang.domain.ProductModel;

public interface ProductDao {

	List<ProductModel> queryByCondition(String queryString, String catalog_name, String price, String sort);

}
