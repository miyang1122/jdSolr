package com.miyang.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.miyang.dao.ProductDao;
import com.miyang.domain.ProductModel;

@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	private SolrServer solrServer;
	
	public List<ProductModel> queryByCondition(String queryString, String catalog_name, String price, String sort) {
		
		SolrQuery solrQuery= new SolrQuery();
		//查询对象
		if(queryString!=null && !"".equals(queryString)){
			solrQuery.setQuery(queryString);
		}
		//默认搜索域
		solrQuery.set("df","product_keywords");
		
		//设置过滤条件
		
		if(catalog_name!=null && !"".equals(catalog_name)){
			solrQuery.setFilterQueries("product_catalog_name:"+catalog_name);
		}
		
		//价格排序
		if(price!=null && !"".equals(price)){
			String[] str = price.split("-");
			solrQuery.setFilterQueries("product_price:["+str[0]+" TO "+str[1]+"]");
		}
		
		/**
		 * 如果第一次访问则查询全部
		 */
		if(queryString==null && catalog_name==null && price==null){
			solrQuery.setQuery("product_price:*");
		}
		
		//设置排序条件
		//1:desc ,0:asc
		
		if(!"1".equals(sort)){
			solrQuery.setSort("product_price",ORDER.asc);
		}else{
			solrQuery.setSort("product_price",ORDER.desc);
		}
		
		//设置分页
		solrQuery.setStart(0);
		solrQuery.setRows(16);
		
		//设置结果域中的列表
		solrQuery.setFields("id","product_name","product_price","product_catalog_name","product_picture");
		
		//设置高亮
		solrQuery.setHighlight(true); //打开高亮开关
		solrQuery.addHighlightField("product_name"); //设置高亮域
		solrQuery.setHighlightSimplePre("<span style='color:red'>");
		solrQuery.setHighlightSimplePost("</span>");
		
		
		QueryResponse query=null;
		try {
			query = solrServer.query(solrQuery);
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		
		SolrDocumentList results = query.getResults();
		
		Map<String, Map<String, List<String>>> highlighting = query.getHighlighting();
		
		System.out.println("共查询到:"+results.getNumFound());
		
		List<ProductModel> proList=new ArrayList<ProductModel>();
		
		for (SolrDocument solrDocument : results) {
			
			String product_name="";
			if(solrDocument.get("id")!=null){
				List<String> list = highlighting.get(solrDocument.get("id")).get("product_name");
				
				if(list != null ){
					product_name=list.get(0);
				}else{
					product_name=(String) solrDocument.get("product_name");
				}
			}
			
			ProductModel product= new ProductModel();
			product.setPid((String) solrDocument.get("id"));
			product.setPicture((String) solrDocument.get("product_picture"));
			product.setCatalog_name((String)solrDocument.get("product_catalog_name"));
			product.setPrice((float)solrDocument.get("product_price"));
			product.setName(product_name);
			
			proList.add(product);
		}
		return proList;
	}

}
